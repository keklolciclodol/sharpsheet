﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Models;
using System.Data.Entity;

namespace Library.Controllers
{
    public class HomeController : Controller
    {
        LibraryContext db = new LibraryContext();

        public ActionResult Index()
        {
            var books = db.Book.Include(p => p.User).Include(p => p.Author).Include(p => p.Genre);
            return View(books);
        }
        [HttpGet]
        public ActionResult BookPage(int id)
        {
            ViewBag.User = db.User.Find(db.Book.Find(id).UserId);
            var theBook = db.Book.Where(p => p.Id == id)
                .Include(p => p.Author).Include(p => p.Genre)
                .Include(p => p.Publisher)
                .FirstOrDefault();
            return View(theBook);
        }
        [HttpGet]
        public ActionResult Create()
        {
            SelectList authors = new SelectList(db.Author, "Id", "Name");
            ViewBag.Authors = authors;
            SelectList genres = new SelectList(db.Genre, "Id", "Name");
            ViewBag.Genres = genres;
            SelectList publishers = new SelectList(db.Publisher, "Id", "Name");
            ViewBag.Publishers = publishers;
            return View();
        }
        [HttpPost]
        public ActionResult Create(Book book)
        {
            book.StatusId = 3;
            db.Book.Add(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult CreateAuthor()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateAuthor(Author author)
        {
            db.Author.Add(author);
            db.SaveChanges();
            return RedirectToAction("Create");
        }
        [HttpGet]
        public ActionResult CreateGenre()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateGenre(Genre genre)
        {
            db.Genre.Add(genre);
            db.SaveChanges();
            return RedirectToAction("Create");
        }
        [HttpGet]
        public ActionResult CreatePublisher()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreatePublisher(Publisher publisher)
        {
            db.Publisher.Add(publisher);
            db.SaveChanges();
            return RedirectToAction("Create");
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Book b = db.Book.Find(id);
            if (b != null)
            {
                db.Book.Remove(b);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult EditBook(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Book book = db.Book.Find(id);
            if (book != null)
            {
                SelectList authors = new SelectList(db.Author, "Id", "Name");
                ViewBag.Authors = authors;
                SelectList genres = new SelectList(db.Genre, "Id", "Name");
                ViewBag.Genres = genres;
                SelectList publishers = new SelectList(db.Publisher, "Id", "Name");
                ViewBag.Publishers = publishers;
                return View(book);
            }
            return HttpNotFound();
        }
        [HttpPost]
        public ActionResult EditBook(Book book)
        {
            db.Entry(book).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User user)
        {
            var theUser = db.User.Where(d => d.Name == user.Name && d.Password == user.Password).FirstOrDefault();
            if(theUser != null)
            {
                Session["UserID"] = theUser.Id.ToString();
                Session["UserName"] = theUser.Name.ToString();
                Session["UserBand"] = theUser.BandId;
                return RedirectToAction("Index");
            }
            return View();
        }
        public ActionResult Logout()
        {
            Session["UserID"] = null;
            Session["UserName"] = null;
            Session["UserBand"] = null;
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(User user)
        {
            user.BandId = 1;
            db.User.Add(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Armor(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            db.Book.Find(id).StatusId = 1;
            db.Book.Find(id).UserId = Convert.ToInt16(Session["UserID"]);
            {
                //db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("BookPage/" + id);
        }
        [HttpGet]
        public ActionResult Give(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            db.Book.Find(id).StatusId = 2;
            {
                //db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("BookPage/" + id);
        }
        [HttpGet]
        public ActionResult Get(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            db.Book.Find(id).StatusId = 3;
            db.Book.Find(id).UserId = null;
            {
                //db.Entry(book).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("BookPage/" + id);
        }
    }
}