﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Library.Models
{
    public class LibraryContext : DbContext
    {
        public LibraryContext() : base("name=Library")
        {
            Database.SetInitializer<LibraryContext>(new LibraryDbInitializer());
        }
        public DbSet<Author> Author { get; set; }
        public DbSet<Band> Band { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<Genre> Genre { get; set; }
        public DbSet<Publisher> Publisher { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<User> User { get; set; }
    }
}