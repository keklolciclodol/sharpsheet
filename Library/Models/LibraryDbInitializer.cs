﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Library.Models
{
    public class LibraryDbInitializer : CreateDatabaseIfNotExists<LibraryContext>
    {
        protected override void Seed(LibraryContext db)
        {
            db.Status.Add(new Status { Name = "Забронированна", Id = 1 });
            db.Status.Add(new Status { Name = "На руках", Id = 2 });
            db.Status.Add(new Status { Name = "Свободна", Id = 3 });
            base.Seed(db);
            db.Band.Add(new Band { Name = "user", Id = 1 });
            db.Band.Add(new Band { Name = "keeper", Id = 2 });
            db.Band.Add(new Band { Name = "admin", Id = 3 });
            base.Seed(db);
            db.User.Add(new User { Name = "kipar", Password = "80085", BandId = 2, Id = 1 });
            db.User.Add(new User { Name = "uzir", Password = "12345", BandId = 1, Id = 2 });
            base.Seed(db);
            db.Author.Add(new Author { Name = "Л. Толстой", Id = 1 });
            db.Author.Add(new Author { Name = "И. Тургенев", Id = 2 });
            db.Author.Add(new Author { Name = "А. Чехов", Id = 3 });
            base.Seed(db);
            db.Genre.Add(new Genre { Name = "роман-эпопея", Id = 1 });
            db.Genre.Add(new Genre { Name = "роман", Id = 2 });
            db.Genre.Add(new Genre { Name = "пьеса", Id = 3 });
            base.Seed(db);
            db.Publisher.Add(new Publisher { Name = "Бука", Id = 1 });
            db.Publisher.Add(new Publisher { Name = "Акелла", Id = 2 });
            base.Seed(db);
            db.Book.Add(new Book { Name = "Война и мир", AuthorId = 1, GenreId = 1, PublisherId = 1, Description = "«Война́ и мир» — роман-эпопея Льва Николаевича Толстого, описывающий русское общество в эпоху войн против Наполеона в 1805—1812 годах.", StatusId = 3, Id = 1 });
            db.Book.Add(new Book { Name = "Отцы и дети", AuthorId = 2, GenreId = 2, PublisherId = 2, Description = "«Отцы́ и де́ти» — роман русского писателя Ивана Сергеевича Тургенева, написанный в 60-е годы XIX века. Роман стал знаковым для своего времени, а образ главного героя Евгения Базарова был воспринят молодёжью как пример для подражания.", StatusId = 3, Id = 2 });
            db.Book.Add(new Book { Name = "Чайка", AuthorId = 3, GenreId = 3, PublisherId = 1, Description = "«Ча́йка» — пьеса в четырёх действиях Антона Павловича Чехова, написанная в 1895—1896 годах и впервые опубликованная в журнале «Русская мысль», в № 12 за 1896 год.", StatusId = 3, Id = 3 });
            base.Seed(db);
        }
    }
}