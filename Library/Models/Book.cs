﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Models
{
    public class Book
    {
        public int Id { get; set; }
        public int? AuthorId { get; set; }
        public Author Author { get; set; }
        public int? GenreId { get; set; }
        public Genre Genre { get; set; }
        public int? PublisherId { get; set; }
        public Publisher Publisher { get; set; }
        public int? StatusId { get; set; }
        public Status Status { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}